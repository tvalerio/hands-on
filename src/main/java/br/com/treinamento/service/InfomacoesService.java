package br.com.treinamento.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.treinamento.representation.CharacterWrapper;
import br.com.treinamento.service.helper.ApplicationCache;

@Component
public class InfomacoesService {

    @Autowired
    private ApplicationCache cache;

    @SuppressWarnings("unchecked")
    public List<CharacterWrapper> recuperarInformacoes() {
        if (cache.dataPresentInCache()) {
            return (List<CharacterWrapper>) cache.getDataFromCache()
                    .getObjectValue();
        }
        return new ArrayList<>();
    }
}
