package br.com.treinamento.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import br.com.treinamento.exception.ServiceException;
import br.com.treinamento.representation.CharacterRepresentation;
import br.com.treinamento.representation.CharacterWrapper;
import br.com.treinamento.representation.ResponseCharacterDataRepresentation;
import br.com.treinamento.representation.ResponseComicDataRepresentation;
import br.com.treinamento.service.helper.ApplicationCache;
import br.com.treinamento.service.helper.MarvelRequestSender;

@Component
public class MarvelService {

    private static final String URL_CHARACTER = "http://gateway.marvel.com:80/v1/public/characters";

    @Autowired
    private MarvelRequestSender marvelRequestSender;

    @Autowired
    private ApplicationCache cache;

    public void buscarInformacoesCharacter(String nomeCharacter)
            throws ServiceException, UnsupportedEncodingException {

        if (Objects.isNull(nomeCharacter)) {
            throw new ServiceException(
                    "Parametro [nomeCharacter] nao informado.");
        }

        List<CharacterWrapper> wrappers = new ArrayList<>();

        String responseCharacter = buscarCharacter(nomeCharacter);

        ResponseCharacterDataRepresentation responseCharacterDataRepresentation = new Gson()
                .fromJson(responseCharacter,
                        ResponseCharacterDataRepresentation.class);

        wrappers.addAll(carregarComics(responseCharacterDataRepresentation));

        cache.putDataInCache(wrappers);
    }

    private String buscarCharacter(String nomeCharacter)
            throws UnsupportedEncodingException {
        return marvelRequestSender.sendRequest(URL_CHARACTER + "?name="
                + URLEncoder.encode(nomeCharacter, "UTF-8"));
    }

    private String buscarCharacterComics(Integer characterId) {
        return marvelRequestSender
                .sendRequest(URL_CHARACTER + "/" + characterId + "/comics");
    }

    private List<CharacterWrapper> carregarComics(
            ResponseCharacterDataRepresentation responseCharacterDataRepresentation) {
        List<CharacterWrapper> wrappersToReturn = new ArrayList<>();

        for (CharacterRepresentation character : responseCharacterDataRepresentation
                .getData().getResults()) {
            CharacterWrapper wrapper = new CharacterWrapper();
            wrapper.setCharacterRepresentation(character);
            String responseComics = buscarCharacterComics(character.getId());
            ResponseComicDataRepresentation responseComicDataRepresentation = new Gson()
                    .fromJson(responseComics,
                            ResponseComicDataRepresentation.class);
            if (responseComicDataRepresentation.getData().getResults() != null
                    && !responseComicDataRepresentation.getData().getResults()
                            .isEmpty()) {
                wrapper.setComicsRepresentations(
                        responseComicDataRepresentation.getData().getResults());
                wrappersToReturn.add(wrapper);
            }
        }

        return wrappersToReturn;
    }
}
