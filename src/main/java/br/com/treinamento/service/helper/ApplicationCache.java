package br.com.treinamento.service.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

@Component
public class ApplicationCache {

    @Autowired
    private CacheManager cacheManager;

    public void putDataInCache(Object object) {
        Cache cache = cacheManager.getCache("dados");

        if (cache.isKeyInCache("1")) {
            cache.remove("1");
        }

        cache.put(new Element("1", object));
    }

    public Element getDataFromCache() {
        Cache cache = cacheManager.getCache("dados");
        return cache.get("1");
    }

    public Boolean dataPresentInCache() {
        return cacheManager.getCache("dados").isKeyInCache("1");
    }
}
