package br.com.treinamento.service.helper;

import java.sql.Timestamp;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

@Component
public class MarvelRequestSender {

    private static final String publicKey = "7d326b30c9c59091ad15373502c35f75";

    private static final String privateKey = "d933e7ea26240d9e20ce2e77c9eb77764c2be0b1";

    public String sendRequest(String url) {
        try {
            Timestamp ts = new Timestamp(new Date().getTime());
            String hash = ts + privateKey + publicKey;
            String md5Hash = DigestUtils.md5DigestAsHex(hash.getBytes())
                    .toString();

            ClientConfig config = new ClientConfig();
            Client client = ClientBuilder.newClient(config);

            String response = client.target(url).queryParam("ts", ts)
                    .queryParam("apikey", publicKey).queryParam("hash", md5Hash)
                    .request("application/json").get(String.class);

            return response;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
