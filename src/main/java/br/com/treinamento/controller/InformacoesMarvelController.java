package br.com.treinamento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.service.MarvelService;

@RestController
public class InformacoesMarvelController {

    @Autowired
    private MarvelService marvelService;

    @RequestMapping(value = "/character", method = RequestMethod.GET, produces = {
            "text/plain; charset=UTF-8" })
    public ResponseEntity<String> buscarCharacterPorNome(
            @RequestParam(value = "nomeCharacter") String nomeCharacter) {
        try {
            marvelService.buscarInformacoesCharacter(nomeCharacter);
        } catch (Exception e) {
            return new ResponseEntity<String>(
                    "Nao foi possivel carregar as informacoes",
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>("Requisicao processada com sucesso",
                HttpStatus.OK);
    }
}
