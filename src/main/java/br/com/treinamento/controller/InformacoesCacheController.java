package br.com.treinamento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import br.com.treinamento.service.InfomacoesService;

@RestController
public class InformacoesCacheController {

    @Autowired
    private InfomacoesService infomacoesService;

    @RequestMapping(value = "/informacoesConsolidadas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> buscarCharacterPorNome() {
        try {
            String json = new Gson()
                    .toJson(infomacoesService.recuperarInformacoes());
            return new ResponseEntity<String>(json, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>(
                    "Nao foi possivel carregar as informacoes",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
