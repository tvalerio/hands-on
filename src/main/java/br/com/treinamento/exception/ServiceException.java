package br.com.treinamento.exception;

public class ServiceException extends Exception {

    private static final long serialVersionUID = 3298475561068528155L;

    public ServiceException(String message) {
        super(message);
    }
}
