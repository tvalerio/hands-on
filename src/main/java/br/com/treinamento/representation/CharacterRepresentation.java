package br.com.treinamento.representation;

import java.io.Serializable;

public class CharacterRepresentation implements Serializable {

    private static final long serialVersionUID = 1698320361154259740L;

    private Integer id;

    private String name;

    private String description;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
