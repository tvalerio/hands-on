package br.com.treinamento.representation;

import java.util.List;

public class DataCharacterRepresentation {

    private String offset;

    private String limit;

    private String total;

    private String count;

    private List<CharacterRepresentation> results;

    public List<CharacterRepresentation> getResults() {
        return results;
    }
}
