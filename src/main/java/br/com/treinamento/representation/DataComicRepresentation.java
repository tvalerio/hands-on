package br.com.treinamento.representation;

import java.util.List;

public class DataComicRepresentation {

    private String offset;

    private String limit;

    private String total;

    private String count;

    private List<ComicRepresentation> results;

    public List<ComicRepresentation> getResults() {
        return results;
    }
}
