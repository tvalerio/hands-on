package br.com.treinamento.representation;

import java.io.Serializable;

public class ComicRepresentation implements Serializable {

    private static final long serialVersionUID = 3261384102699122842L;

    private Integer id;

    private String title;

    private String description;

    private String pageCount;
}
