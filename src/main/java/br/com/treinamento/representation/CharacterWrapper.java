package br.com.treinamento.representation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CharacterWrapper implements Serializable {

    private static final long serialVersionUID = 3043271314322965020L;

    private CharacterRepresentation characterRepresentation;

    private List<ComicRepresentation> comicsRepresentations;

    public CharacterWrapper() {
        comicsRepresentations = new ArrayList<>();
    }

    public CharacterRepresentation getCharacterRepresentation() {
        return characterRepresentation;
    }

    public void setCharacterRepresentation(
            CharacterRepresentation characterRepresentation) {
        this.characterRepresentation = characterRepresentation;
    }

    public List<ComicRepresentation> getComicsRepresentations() {
        return comicsRepresentations;
    }

    public void setComicsRepresentations(
            List<ComicRepresentation> comicsRepresentations) {
        this.comicsRepresentations = comicsRepresentations;
    }
}
