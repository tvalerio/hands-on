package br.com.treinamento.service.test;

import java.util.List;
import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.representation.CharacterWrapper;
import br.com.treinamento.service.InfomacoesService;
import br.com.treinamento.service.MarvelService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfig.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InformacoesServiceTest {

    @Autowired
    private MarvelService marvelService;

    @Autowired
    private InfomacoesService infomacoesService;

    @Test
    public void deveRetornarDadosVaziosSeAPICarregamentoNaoChamada()
            throws Exception {
        List<CharacterWrapper> wrappers = infomacoesService
                .recuperarInformacoes();
        Assert.assertTrue(wrappers.isEmpty());
    }

    @Test
    public void deveRetornarDadosWrapperCasoAPICarregamentoEncontreDados()
            throws Exception {
        marvelService.buscarInformacoesCharacter("Spider-Man");
        List<CharacterWrapper> wrappers = infomacoesService
                .recuperarInformacoes();
        Assert.assertTrue(Objects.nonNull(wrappers));
        Assert.assertTrue(wrappers.size() > 0);
        Assert.assertTrue(wrappers.get(0).getCharacterRepresentation().getName()
                .equals("Spider-Man"));
        Assert.assertTrue(
                wrappers.get(0).getComicsRepresentations().size() > 0);
    }
}
