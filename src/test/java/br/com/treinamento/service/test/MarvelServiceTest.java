package br.com.treinamento.service.test;

import java.util.List;
import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.exception.ServiceException;
import br.com.treinamento.representation.CharacterWrapper;
import br.com.treinamento.service.MarvelService;
import br.com.treinamento.service.helper.ApplicationCache;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfig.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MarvelServiceTest {

    @Autowired
    private MarvelService marvelService;

    @Autowired
    private ApplicationCache cache;

    @Test
    public void deveBuscarInformacoesMarvelColocandoNoCache() throws Exception {
        marvelService.buscarInformacoesCharacter("Spider-man");

        Assert.assertTrue(cache.dataPresentInCache());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void deveBuscarInformacoesMarvelEGerarWrapper() throws Exception {
        marvelService.buscarInformacoesCharacter("Spider-Man");
        List<CharacterWrapper> characters = (List<CharacterWrapper>) cache
                .getDataFromCache().getObjectValue();
        Assert.assertTrue(Objects.nonNull(characters));
        Assert.assertTrue(characters.size() > 0);
        Assert.assertTrue(characters.get(0).getCharacterRepresentation()
                .getName().equals("Spider-Man"));
        Assert.assertTrue(
                characters.get(0).getComicsRepresentations().size() > 0);
    }

    @Test(expected = ServiceException.class)
    public void deveGerarExceptionCasoNomeCharacterSejaNull() throws Exception {
        marvelService.buscarInformacoesCharacter(null);
    }
}
