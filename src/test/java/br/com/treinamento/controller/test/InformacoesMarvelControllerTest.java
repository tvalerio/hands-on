package br.com.treinamento.controller.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.config.MVCConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class, MVCConfig.class })
@WebAppConfiguration
public class InformacoesMarvelControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    protected MockMvc mockMvc;

    @Before
    public void init() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void deveCarregarInformacoesMarvelViaAPI() throws Exception {
        this.mockMvc
                .perform(get("/character").param("nomeCharacter", "Spider-Man"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain; charset=UTF-8"))
                .andExpect(jsonPath("$")
                        .value("Requisicao processada com sucesso"));
    }

    @Test
    public void naoDeveCarregarInformacoesMarvelViaAPIComNomeVazio()
            throws Exception {
        this.mockMvc.perform(get("/character?nomeCharacter="))
                .andExpect(status().isBadRequest());
    }
}
