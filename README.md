# README #

### How do I get set up? ###

* Sumary
 - A funcionalidade adotada para atender o desafio foi bem simples: Basicamente, chamo uma API da marvel para buscar um Character por nome e caso encontre, chamo outra API para buscar as 20 primeiras Comics deste Character, colocando alguns dados selecionados (não todos) para retornar na segunda API requisitada.



* Necessário Java 8 e maven para rodar

* Informações da API presentes no endereço http://localhost:8080/swagger-ui.html após subir a aplicação.
 - Utilizar a API /character passando o nome desejado para procurar um Character qualquer. Exemplo: Spider-Man, Iron Man, etc.
 - Utilizar a API /informacoesConsolidadas para retornar os dados encontrados

* Para subir a aplicação: mvn spring-boot:run

* Para rodar os testes com relatório de cobertura de testes do jacoco: mvn clean test jacoco:report